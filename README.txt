First. make sure you are running mysql server and create a database schema called "medical", and set your database credentials in the application.properties file.

1. Server
Considering you are using Intellij, open the server folder as an Intellij Project, click Build and run the application.

2. Client (Angular)
With the server running, open the angular_client folder as a Webstorm Project. Otherwise, open a terminal in the angular_client folder and run
"ng serve" for starting up the angular dev server. Run "ng build" to build the project.