import { Component, OnInit } from '@angular/core';
import {Patient} from "../model/Patient";
import {UserService} from "../service/user-service";
import {LoginService} from "../service/login.service";
import {Caregiver} from "../model/Caregiver";
import {HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-caregiver-page',
  templateUrl: './caregiver-page.component.html',
  styleUrls: ['./caregiver-page.component.css']
})
export class CaregiverPageComponent implements OnInit {

  patients:Patient[];
  caregiver:Caregiver;
  headers:HttpHeaders;
  constructor(private userService:UserService) {
    this.headers= new HttpHeaders();
    this.headers=this.headers.append("role","caregiver");
  }

  ngOnInit() {
    this.caregiver=JSON.parse(localStorage.getItem('user'));
    this.userService.getPatientsForCaregiver(this.caregiver.id,this.headers).subscribe(data=>{
      this.patients=data;
    });
  }

}
