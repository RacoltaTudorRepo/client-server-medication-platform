import { Component, OnInit } from '@angular/core';
import {Medication} from "../model/Medication";
import {MedicationService} from "../service/medication.service";
import {Patient} from "../model/Patient";
import {UserService} from "../service/user-service";
import {MedicationPlan} from "../model/MedicationPlan";
import {ActivatedRoute,Router} from "@angular/router";
import {HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-create-medication-plan-page',
  templateUrl: './create-medication-plan-page.component.html',
  styleUrls: ['./create-medication-plan-page.component.css']
})
export class CreateMedicationPlanPageComponent implements OnInit {

  medications=[];
  medication:Medication;
  sideEffects:string="";
  medicationAvailable:Medication[]=[];

  patients=[];
  patientsAvailable:Patient[]=[];

  intakeIntervals:string="";
  medicationPlan:MedicationPlan;

  headers:HttpHeaders;

  constructor(private medicationService:MedicationService,private userService:UserService,private router:Router) {
    this.medication=new Medication();
    this.medicationPlan=new MedicationPlan();
    this.headers= new HttpHeaders();
    this.headers=this.headers.append("role","doctor");
    this.headers=this.headers.append("medication","true");
  }

  ngOnInit() {
    this.medicationService.getAvailableMedications(this.headers).subscribe(data => {
      for(let medication of data){
        if(medication.inPlan==false)
          this.medicationAvailable.push(medication)
      }
      for (let medication of this.medicationAvailable)
        this.medications.push({medication: medication, isChecked: false});
    });

    this.userService.findPatients(this.headers).subscribe(data => {
      this.patientsAvailable = data;
      for (let patient of this.patientsAvailable)
        this.patients.push({patient: patient, isChecked: false});
    });
  }



  confirmMedication() {
    this.medication.sideEffects = this.sideEffects.split(",");
    if(this.medication.name==undefined || this.medication.dosage==undefined){
      window.alert("Fill in medication name and dosage");
      return;
    }
    this.medicationService.addMedication(this.medication,this.headers).subscribe().add(()=>{
      this.router.navigate(['/doctorPage/createMedicationPlan']);
      console.log("nav")
    });
  }

  checkEvt(medicationName:string){
    for(let x=0;x<this.medications.length;x++){
      if(this.medications[x].medication.name===medicationName) {
        this.medications[x].isChecked = !this.medications[x].isChecked;
      }
    }
    console.log(this.medications);
  }

  checkEvtPatient(patientName:string){
    for(let x=0;x<this.patients.length;x++){
      if(this.patients[x].patient.name===patientName) {
        this.patients[x].isChecked = !this.patients[x].isChecked;
      }
    }
    console.log(this.patients);
  }

  confirmMedicationPlan(){
    let medications:Medication[]=[];
    let patientSelected:Patient;
    let pickedOne=false;
    for(let medication of this.medications)
      if(medication.isChecked)
          medications.push(medication.medication);
    if(medications.length==0){
      window.alert("Please select medication first!")
      return;
    }
    for(let patient of this.patients)
      if(patient.isChecked) {
        if(pickedOne==false){
          patientSelected=patient.patient;
         pickedOne=true;
        }
        else {
          window.alert("Please pick only one patient");
          return;
        }
      }

    if(patientSelected==undefined){
      window.alert("Please pick one patient");
      return;
    }

    let medicationPlan:MedicationPlan=new MedicationPlan();
    medicationPlan.period=this.medicationPlan.period;
    medicationPlan.intakeIntervals=this.intakeIntervals.split(",");
    medicationPlan.patientID=patientSelected.id;
    for(let medication of medications)
      medication.inPlan=true;
    medicationPlan.medication=medications;


    console.log(medicationPlan);
    this.medicationService.addMedicationPlan(medicationPlan,this.headers).subscribe();
    this.router.navigate(['/doctorPage/createMedicationPlan'])
  }
}
