import { Component, OnInit } from '@angular/core';
import {User} from "../model/User";
import {UserService} from "../service/user-service";
import {Patient} from "../model/Patient";
import {Caregiver} from "../model/Caregiver";
import {HttpHeaders} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit{
  user:User;
  role:string;
  medicalRecord:string;
  patients=[];
  headers:HttpHeaders;
  constructor(private userService:UserService,private router:Router) {
    this.user=new User();
    this.headers= new HttpHeaders();
    this.headers=this.headers.append("role","doctor");
  }

  ngOnInit(): void {
   this.userService.findPatients(this.headers).subscribe(data=>{
     console.log(data);
      for(let obj of data){
        if(obj.cared==false){
          this.patients.push({patient: obj, isChecked: false});
        }
      }
    }).add(()=>{
      console.log(this.patients);
   });
  }

  setRole(role:string){
    this.role=role;
  }

  checkEvt(patientName:string){
    for(let x=0;x<this.patients.length;x++){
      if(this.patients[x].patient.name===patientName) {
        this.patients[x].isChecked = !this.patients[x].isChecked;
      }
    }
    console.log(this.patients);
  }

  confirmUser(){
    if(this.role==='Patient'){
      let patient:Patient=new Patient(this.user.id,this.user.username,this.user.name,this.user.password,this.user.birthDate,this.user.gender,this.user.adress);
      patient.medicalRecord=this.medicalRecord;
      this.userService.addUser(patient,'Patient',this.headers).subscribe().add(()=>{
        this.router.navigate(['doctorPage'])
      });
    }
    else if(this.role==='Caregiver'){
      let patients:Patient[]=[];
      let caregiver:Caregiver=new Caregiver(this.user.id,this.user.username,this.user.name,this.user.password,this.user.birthDate,this.user.gender,this.user.adress);
      for(let patient of this.patients)
        if(patient.isChecked)
          patients.push(patient.patient);
      caregiver.patients=patients;
      this.userService.addCaregiver(caregiver,this.headers).subscribe().add(()=>{
        this.router.navigate(['doctorPage']);
      });
      console.log("create");
    }

  }

}
