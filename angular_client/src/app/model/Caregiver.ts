import {Patient} from "./Patient";
import {User} from "./User";

export class Caregiver extends User{
    patients:Patient[];
    constructor(id?: number, username?: string, name?: string, password?: string, birthDate?: string, gender?: string, adress?: string){
        super(id,username,name,password,birthDate,gender,adress);
        this.userROLE="Caregiver";
    }
}