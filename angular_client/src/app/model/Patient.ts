import {User} from "./User";

export class Patient extends User{
    medicalRecord:string;
    cared:boolean;
    constructor(id?: number, username?: string, name?: string, password?: string, birthDate?: string, gender?: string, adress?: string){
        super(id,username,name,password,birthDate,gender,adress);
        this.userROLE="Patient";
    }


}