export class User{
    id:number;
    username:string;
    name: string;
    password:string;
    birthDate:string;
    gender:string;
    adress:string;
    userROLE:string;


    constructor(id?: number, username?: string, name?: string, password?: string, birthDate?: string, gender?: string, adress?: string) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.password = password;
        this.birthDate = birthDate;
        this.gender = gender;
        this.adress = adress;
    }
}
