import { Injectable } from '@angular/core';
import {User} from "../model/User";
import {HttpClient,HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Patient} from "../model/Patient";
import {Caregiver} from "../model/Caregiver";
import {Doctor} from "../model/Doctor";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private doctorURL:string;
  private caregiverURL:string;
  private patientURL:string;

  constructor(private http:HttpClient) {
    this.doctorURL="http://localhost:8080/backend/doctor";
    this.caregiverURL="http://localhost:8080/backend/caregiver";
    this.patientURL="http://localhost:8080/backend/patient";
  }

  public findPatients(headers:HttpHeaders):Observable<Patient[]>{
    return this.http.get<Patient[]>(this.doctorURL+"/getPatients",{headers:headers});
  }

  public findCaregivers(headers:HttpHeaders):Observable<Caregiver[]>{
    return this.http.get<Caregiver[]>(this.doctorURL+"/getCaregivers",{headers:headers});
  }

  public addUser(user:User,userRole:string,headers:HttpHeaders){
    return this.http.post<User>(this.doctorURL+"/create"+userRole,user,{headers:headers});
  }

  public addCaregiver(user:Caregiver,headers:HttpHeaders){
    return this.http.post<User>(this.doctorURL+"/createCaregiver",user,{headers:headers});
  }

  public deleteUser(user:User,role:String,headers:HttpHeaders){
    return this.http.post<User>(this.doctorURL+"/delete"+role,user,{headers:headers});
  }

  public modifyPatient(patient:Patient,headers:HttpHeaders){
    return this.http.post<Patient>(this.doctorURL+"/updatePatient",patient,{headers:headers});
  }

  public getCaregiver(name:String,headers:HttpHeaders){
    return this.http.post<Caregiver>(this.caregiverURL+"/getCaregiver",name,{headers:headers});
  }

  public getPatient(name:string,headers:HttpHeaders){
    return this.http.post<Patient>(this.patientURL+"/getPatient",name,{headers:headers});
  }

  public getDoctor(name:string,headers:HttpHeaders){
    return this.http.post<Doctor>(this.doctorURL+"/getDoctorByUsername",name,{headers:headers});
  }

  public modifyCaregiver(user:Caregiver,headers:HttpHeaders){
    return this.http.post<User>(this.doctorURL+"/updateCaregiver",user,{headers:headers});
  }

  public uncarePatients(uncared:Patient[],headers:HttpHeaders){
    return this.http.post<User>(this.doctorURL+"/uncarePatients",uncared,{headers:headers});
  }

  public getPatientsForCaregiver(caregiverID:number,headers:HttpHeaders){
    return this.http.post<Patient[]>(this.caregiverURL+"/getPatientsForCaregiver",caregiverID,{headers:headers});
  }
}
