import { Component, OnInit } from '@angular/core';
import {UserService} from "../service/user-service";
import {Patient} from "../model/Patient";
import {HttpHeaders} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-user-modify',
  templateUrl: './user-modify.component.html',
  styleUrls: ['./user-modify.component.css']
})
export class UserModifyComponent implements OnInit {
  user:any;
  patients=[];
  headers:HttpHeaders;

  constructor(private userService:UserService, private router:Router) {
    this.headers= new HttpHeaders();
    this.headers=this.headers.append("role","doctor");
  }

  ngOnInit() {
    this.user=JSON.parse(localStorage.getItem('user_modify'));
    console.log(this.user);
    if(this.user.userROLE=='Caregiver') {
      this.userService.findPatients(this.headers).subscribe(data => {
        for (let obj of data) {
          if (obj.cared == false) {
            this.patients.push({patient: obj, isChecked: false});
          }
        }
      }).add(() => {
        for (let patient of this.user.patients) {
          this.patients.push({patient: patient, isChecked: true});
        }
        console.log(this.patients);
      });
    }
  }

  checkEvt(patientName:string){
    for(let x=0;x<this.patients.length;x++){
      if(this.patients[x].patient.name===patientName) {
        this.patients[x].isChecked = !this.patients[x].isChecked;
      }
    }
    console.log(this.patients);
  }

  confirmModify(){
    if(this.user.userROLE==='Patient'){
      console.log('here');
      this.userService.modifyPatient(this.user,this.headers).subscribe(data=>{},err=>{
        window.alert("Invalid date entered!");
        return;
      }).add(()=>{
        this.router.navigate(['doctorPage']);
      });}
    else {
      let patients:Patient[]=[];
      let uncared:Patient[]=[];
      for(let patient of this.patients)
        if(patient.isChecked)
          patients.push(patient.patient);
        else
          uncared.push(patient.patient);
      this.user.patients=patients;
      this.userService.uncarePatients(uncared,this.headers).subscribe().add(()=>{
        this.userService.modifyCaregiver(this.user,this.headers).subscribe(data=>{},error => {
          window.alert("Invalid date entered!");
          return;
        }).add(()=>{
          this.router.navigate(['doctorPage']);
        });
      });
    }
  }


}
