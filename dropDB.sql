DROP table medical.user;
DROP table medical.medication_plan_intake_intervals;
DROP table medical.medication_side_effects;
DROP table medical.medication;
DROP table medical.medication_plan;
DROP table medical.hibernate_sequence;