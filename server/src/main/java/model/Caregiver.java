package model;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@DiscriminatorColumn(name="CAREGIVER")
public class Caregiver extends User{

    @JoinColumn(name="caregiver_id")
    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.MERGE)
    private List<Patient> patients;

    public Caregiver() {
    }

    public Caregiver(String username, String password, String name, Date birthDate, String gender, String adress,List<Patient> patients) {
        super(username,password, name, birthDate, gender, adress,"Caregiver");
        this.patients=patients;
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }
}
