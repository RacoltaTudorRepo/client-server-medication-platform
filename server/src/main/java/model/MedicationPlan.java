package model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
public class MedicationPlan {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long ID;

    private Long patientID;

    @OneToMany(fetch=FetchType.EAGER)
    @JoinColumn(name="medication_plan_ID")
    private List<Medication> medication;

    @ElementCollection(fetch=FetchType.EAGER)
    private Set<String> intakeIntervals;
    private int period;

    public MedicationPlan() {
    }

    public MedicationPlan(Long patientID, List<Medication> medication, Set<String> intakeIntervals, int period) {
        this.patientID = patientID;
        this.medication = medication;
        this.intakeIntervals = intakeIntervals;
        this.period = period;
    }

    public Long getPatientID() {
        return patientID;
    }

    public void setPatientID(Long patientID) {
        this.patientID = patientID;
    }

    public List<Medication> getMedication() {
        return medication;
    }

    public void setMedication(List<Medication> medication) {
        this.medication = medication;
    }

    public Set<String> getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(Set<String> intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    @Override
    public String toString() {
        return "MedicationPlan{" +
                "patientID=" + patientID +
                ", medication=" + medication +
                ", intakeIntervals=" + intakeIntervals +
                ", period=" + period +
                '}';
    }
}
