package repository;

import model.Caregiver;
import org.springframework.data.repository.CrudRepository;

public interface CaregiverRepository extends CrudRepository<Caregiver,Long> {
    Caregiver findCaregiverByUsername(String username);
}
