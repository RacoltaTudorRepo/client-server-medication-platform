package repository;

import model.Patient;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PatientRepository extends CrudRepository<Patient,Long> {
    @Query("SELECT u from Patient u where caregiver_id=?1")
    List<Patient> findAllBycaregiver_id(Long id);

    @Query("Select u from Patient u where id=?1")
    Patient findByID(Long id);

    Patient findPatientByUsername(String name);
}
