package repository;
import java.util.ArrayList;
import java.util.List;

import model.Patient;
import model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long>{
    List<User> findUserByUserROLE(String userROLE);
}

